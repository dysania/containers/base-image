#!/bin/bash

set -x
set -e

newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount $newcontainer)
mount -i -o remount,dev $scratchmnt
debootstrap --variant=minbase testing $scratchmnt
buildah unmount $newcontainer
buildah commit $newcontainer "${REGISTRY_URL}/mkeedlinger/base-image:rolling"
podman login -u $REGISTRY_USER -p $REGISTRY_SECRET $REGISTRY_URL
podman push "${REGISTRY_URL}/mkeedlinger/base-image:rolling"
