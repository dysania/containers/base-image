# base-image
This repo is for mkeedlinger's custom pressed base-images built with [buildah](https://buildah.io/)

## Tags
The naming for the tags is modelled after each distros respective DockerHub repo.

- latest: Debian stable
- rolling: Debian testing
- fedora-latest: latest Fedora
- ubuntu-latest: Ubuntu LTS
- ubuntu-rolling: latest Ubuntu