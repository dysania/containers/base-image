#!/bin/bash

set -x
set -e

newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount $newcontainer)
mount -i -o remount,dev $scratchmnt
dnf install --installroot $scratchmnt --releasever 31 bash coreutils --setopt install_weak_deps=false -y
buildah unmount $newcontainer
buildah commit $newcontainer "${REGISTRY_URL}/mkeedlinger/base-image:fedora-latest"
podman login -u $REGISTRY_USER -p $REGISTRY_SECRET $REGISTRY_URL
podman push "${REGISTRY_URL}/mkeedlinger/base-image:fedora-latest"
